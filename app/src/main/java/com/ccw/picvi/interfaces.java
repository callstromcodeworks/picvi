package com.ccw.picvi;

import android.view.ContextMenu;
import android.view.View;

public final class interfaces {
    private interfaces() { throw new AssertionError(); }

    public interface onItemClickListener {
        void onItemClick(View v, int position);
    }
    public interface onCreateContextMenuListener {
        void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo info);
    }
}
