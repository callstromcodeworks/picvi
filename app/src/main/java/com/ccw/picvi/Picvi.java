package com.ccw.picvi;

import android.app.Application;
import android.content.Intent;

import androidx.core.content.pm.ShortcutInfoCompat;
import androidx.core.content.pm.ShortcutManagerCompat;

import com.ccw.picvi.activity.Main;
import com.ccw.picvi.service.SyncAgent;

import java.util.List;
import java.util.UUID;

public class Picvi extends Application {

    // Application wide constants
    public static final String SERVER_URL = "https://dev.strike527.com";

    public static SyncAgent sync;

    @Override
    public void onCreate() {
        super.onCreate();
        sync = SyncAgent.getInstance(this);
        checkShortcuts();
    }

    private void checkShortcuts() {
        List<ShortcutInfoCompat> listAll = ShortcutManagerCompat.getShortcuts(getApplicationContext(),
                ShortcutManagerCompat.FLAG_MATCH_CACHED|
                        ShortcutManagerCompat.FLAG_MATCH_DYNAMIC|
                        ShortcutManagerCompat.FLAG_MATCH_MANIFEST|
                        ShortcutManagerCompat.FLAG_MATCH_PINNED);
        if (listAll.size() > 0) {

        } else {
            Intent i = new Intent(this, Main.class);
            i.setAction(Intent.ACTION_SEND);
            ShortcutInfoCompat sendShortcut = new ShortcutInfoCompat.Builder(this, UUID.randomUUID().toString())
                    .setShortLabel("Picvi: Compose")
                    .setIntent(i)
                    .build();
            ShortcutManagerCompat.pushDynamicShortcut(this, sendShortcut);
        }
    }


}
