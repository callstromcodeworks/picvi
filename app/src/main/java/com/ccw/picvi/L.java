package com.ccw.picvi;

import android.util.Log;

public final class L {
    public static final String TAG = "CCW_DEBUG";

    public static void d(String msg) { Log.d(TAG, msg); }

    public static void e(String msg) { Log.e(TAG, msg); }

    public static void i(String msg) { Log.i(TAG, msg); }
}
