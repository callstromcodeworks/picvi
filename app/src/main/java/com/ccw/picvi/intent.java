package com.ccw.picvi;

public final class intent {

    private intent() { throw new AssertionError(); }

    public static final String MESSAGE_RECEIVED = "com.ccw.MESSAGE_RECEIVED";
    public static final String OPEN_CONVERSATION = "com.ccw.OPEN_CONVERSATION";
    public static final String COMPOSE_MESSAGE = "com.ccw.COMPOSE_MESSAGE";

    public static final String EXTRA_ID = "_id";
}
