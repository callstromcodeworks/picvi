package com.ccw.picvi.service;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.ccw.picvi.object.Contact;
import com.ccw.picvi.object.Conversation;
import com.ccw.picvi.object.Message;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Used for local data storage
 */
public final class DatabaseHandler extends SQLiteOpenHelper {

    private static DatabaseHandler handler;
    private final SQLiteDatabase db;

    private static final String DATABASE_PATH =  "picvi.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_CONV = "conversations";
    private static final String TABLE_CONT = "contacts";
    private static final String TABLE_MSG = "messages";

    private static final String ID_KEY = "_id";
    private static final String D_NAME_KEY = "displayname";
    private static final String U_NAME_KEY = "username";
    private static final String SNIP_KEY = "snippet";
    private static final String TEXT_KEY = "text";
    private static final String DATE_KEY = "date";

    private static final String[] CONVERSATION_COLUMNS = { ID_KEY, D_NAME_KEY, SNIP_KEY };
    private static final String[] CONTACT_COLUMNS = { ID_KEY, D_NAME_KEY, U_NAME_KEY };
    private static final String[] MSG_COLUMNS = { ID_KEY, D_NAME_KEY, TEXT_KEY };

    private static final String T_INIT = "CREATE TABLE IF NOT EXISTS";
    private static final String ID_DEF = "'" + ID_KEY + "' INTEGER UNIQUE NOT NULL";
    private static final String D_NAME_DEF = "'" + D_NAME_KEY + "' TEXT";
    private static final String U_NAME_DEF = "'" + U_NAME_KEY + "' TEXT";
    private static final String SNIP_DEF = "'" + SNIP_KEY + "' TEXT";
    private static final String TEXT_DEF = "'" + TEXT_KEY + "' TEXT";
    private static final String DATE_DEF = "'" + DATE_KEY + "' INTEGER DEFUALT CURRENT_TIMESTAMP";

    public synchronized static DatabaseHandler getHandler(Context context) {
        if (handler == null) handler = new DatabaseHandler(context);
        return handler;
    }

    private DatabaseHandler (Context context){
        super(context, DATABASE_PATH, null, DATABASE_VERSION);
        this.db = getWritableDatabase();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(generateTableCreateSQL(TABLE_CONV, ID_DEF, D_NAME_DEF, SNIP_DEF));
        db.execSQL(generateTableCreateSQL(TABLE_CONT, ID_DEF, D_NAME_DEF, U_NAME_DEF));
        db.execSQL(generateTableCreateSQL(TABLE_MSG, ID_DEF, D_NAME_DEF, TEXT_DEF));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void onDestroy() {
        close();
    }

    private String generateTableCreateSQL(String table, String... varargs) {
        StringBuilder sql = new StringBuilder(T_INIT);
        sql.append(" ").append(table).append("(");
        for (String s : varargs) {
            sql.append(s);
            sql.append(", ");
        }
        sql.append(DATE_DEF);
        sql.append(");");
        return sql.toString();
    }

    public Collection<? extends Conversation> loadConversations() {
        final List<Conversation> list = new ArrayList<>();
        Cursor c = query(TABLE_CONV, CONVERSATION_COLUMNS);
        if (!c.moveToFirst()) return null;
        int idInd = c.getColumnIndex(ID_KEY), displayInd = c.getColumnIndex(D_NAME_KEY), snipInd = c.getColumnIndex(SNIP_KEY);
        for (int i=0;i<c.getCount();i++) {
            Conversation con = new Conversation(c.getLong(idInd), c.getString(displayInd), c.getString(snipInd));
            list.add(con);
        }
        c.close();
        return list;
    }

    public Collection<? extends Contact> loadContacts() {
        final List<Contact> list = new ArrayList<>();
        Cursor c = query(TABLE_CONT, CONTACT_COLUMNS);
        if (!c.moveToFirst()) return null;
        int idInd = c.getColumnIndex(ID_KEY), displayInd = c.getColumnIndex(D_NAME_KEY), userInd = c.getColumnIndex(U_NAME_KEY);
        for (int i=0;i<c.getCount();i++) {
            Contact con = new Contact(c.getLong(idInd), c.getString(displayInd), c.getString(userInd));
            list.add(con);
        }
        c.close();
        return list;
    }
    public Collection<? extends Message> loadMessages(String username) {
        final List<Message> list = new ArrayList<>();
        Cursor c = query(TABLE_MSG, MSG_COLUMNS, U_NAME_KEY + "=?", username);
        if (!c.moveToFirst()) return null;
        int idInd = c.getColumnIndex(ID_KEY), displayInd = c.getColumnIndex(D_NAME_KEY), textInd = c.getColumnIndex(TEXT_KEY);
        for (int i=0;i<c.getCount();i++) {
            Message con = new Message(c.getLong(idInd), c.getString(displayInd), c.getString(textInd));
            list.add(con);
        }
        c.close();
        return list;
    }


    public Contact getContactInfo(String username) {
        Contact result = new Contact();
        Cursor c = query(TABLE_CONT, CONTACT_COLUMNS, U_NAME_KEY + "=?", username);
        if (!c.moveToFirst()) return null;
        return result;
    }

    /**
     * Convenience method for calling {@link android.database.sqlite.SQLiteDatabase#query(String, String[], String, String[], String, String, String)} with null for everything except:
     *
     * @param table The table name to compile the query against.
     * @param columns A list of which columns to return. Passing null will return all columns, which is discouraged to prevent reading data from storage that isn't going to be used.
   */
    private Cursor query(String table, String[] columns) {
        return db.query(table, columns, null, null, null, null, null);
    }
    /**
     * Convenience method for calling {@link android.database.sqlite.SQLiteDatabase#query(String, String[], String, String[], String, String, String)} with null for groupBy and having
     *
     * @param table The table name to compile the query against.
     * @param columns A list of which columns to return. Passing null will return all columns, which is discouraged to prevent reading data from storage that isn't going to be used.
     * @param selection A filter declaring which rows to return, formatted as an SQL WHERE clause (excluding the WHERE itself). Passing null will return all rows for the given table.
     * @param selectionArgs You may include ?s in selection, which will be replaced by the values from selectionArgs, in order that they appear in the selection. The values will be bound as Strings.
     */
    private Cursor query(String table, String[] columns, String selection, @Nullable String... selectionArgs) {
        return db.query(table, columns, selection, selectionArgs, null, null, "'" + DATE_KEY + "' DESC");
    }
}
