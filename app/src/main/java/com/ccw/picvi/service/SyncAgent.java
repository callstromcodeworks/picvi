package com.ccw.picvi.service;

import android.content.Context;
import android.content.ContextWrapper;

//TODO Setup synchronization w/server
// 1) Contacts
// 2) Messages
// 3) Settings
// -
// We need to favor the server's info over client info
// -
// We also need to determine how often we are doing this, data usage is a concern here
public class SyncAgent extends ContextWrapper {
    private static SyncAgent instance;
    private SyncAgent(Context base) { super(base); }

    public static synchronized SyncAgent getInstance(Context context) {
        if (instance == null) instance = new SyncAgent(context);
        return instance;
    }



}
