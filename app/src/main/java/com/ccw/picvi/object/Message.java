package com.ccw.picvi.object;

public class Message {
    public long id = -1;
    public String displayname, message;
    public byte[] picture;

    public Message(long id, String displayname, String message) {
        this(id, displayname, message, null);
    }

    public Message(long id, String displayname, String message, byte[] picture) {
        this.id = id;
        this.displayname = displayname;
        this.message = message;
        this.picture = picture;
    }
}
