package com.ccw.picvi.object;

public class Contact {
    public long id = -1;
    public String displayname, username;
    public byte[] picture;

    public Contact() {}

    public Contact(long id, String displayname, String username) {
        this(id, displayname, username, null);
    }

    public Contact(long id, String displayname, String username, byte[] picture) {
        this.id = id;
        this.displayname = displayname;
        this.username = username;
        this.picture = picture;
    }
}
