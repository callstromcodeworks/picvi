package com.ccw.picvi.object;

public class Conversation {
    public long id = -1;
    public String name, snippet;
    public byte[] picture;

    public Conversation(long id, String name, String snippet) {
        this(id, name, snippet, null);
    }

    public Conversation(long id, String name, String snippet, byte[] picture) {
        this.id = id;
        this.name = name;
        this.snippet = snippet;
        this.picture = picture;
    }
}
