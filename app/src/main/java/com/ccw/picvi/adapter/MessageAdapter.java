package com.ccw.picvi.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ccw.picvi.service.DatabaseHandler;
import com.ccw.picvi.R;
import com.ccw.picvi.interfaces;
import com.ccw.picvi.object.Message;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private final List<Message> internalList = new ArrayList<>();
    private final LayoutInflater inflater;
    protected interfaces.onItemClickListener l;
    protected interfaces.onCreateContextMenuListener c;
    private final DatabaseHandler db;

    @SuppressWarnings("unused")
    private MessageAdapter(){
        throw new AssertionError();
    }

    public MessageAdapter(Context context, Uri username, interfaces.onItemClickListener listener, interfaces.onCreateContextMenuListener cListener) {
        inflater = LayoutInflater.from(context);
        db = DatabaseHandler.getHandler(context);
        //internalList.addAll(list);
        testSetup();
        l = listener;
        c = cListener;
    }


    public void testSetup() {
        int iteration = -1;
        for (int i=0;i<50;i++) {
            /*
            (3*n)-y=-x
            (3*n)+b=p
            (3*n)=p-b
            (3*n)-p=-b
            x= -x * -1
            n=iteration; p=position=i; b=base;
             */
            if (i % 3 == 0) iteration++;
            int color = ((3 * iteration) - i) * -1;
            Bitmap bm = createBitmap(color);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 20, stream);
            byte[] result = stream.toByteArray();
            bm.recycle();

            String name = color == 0 ? "Red" : color == 1 ? "Green" : "Blue";
            String snip = "n=" +
                    iteration +
                    " p=" +
                    i +
                    " c=" +
                    color;
            Message conv = new Message(i, name, snip, result);
            internalList.add(conv);
        }
    }
    public Bitmap createBitmap(int test) {
        Bitmap bitmap = Bitmap.createBitmap(256, 256, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        int color;
        if (test == 0) {
            color = Color.RED;
        }
        else if (test == 1) {
            color = Color.GREEN;
        }
        else {
            color = Color.BLUE;
        }
        canvas.drawColor(color);
        return bitmap;
    }

    /**
     * Called when this view is created
     * @param parent a reference to the recyclerView parent
     * @param viewType the type of view?
     * @return our view holder
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.recyclerview_row, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.contactPic = itemView.findViewById(R.id.row_picture);
        holder.displayname = itemView.findViewById(R.id.row_name);
        holder.message = itemView.findViewById(R.id.row_snippet);
        holder.container = itemView.findViewById(R.id.row_container);
        return holder;
    }

    /**
     * Called when view is shown on screen
     * @param holder a ViewHolder
     * @param position position
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Message msg = internalList.get(position);
        holder.contactPic.setImageBitmap(BitmapFactory.decodeByteArray(msg.picture, 0, msg.picture.length));
        holder.displayname.setText(msg.displayname);
        holder.message.setText(msg.message);
        holder.container.setOnClickListener(v -> l.onItemClick(v, holder.getAdapterPosition()));
        holder.container.setOnCreateContextMenuListener((menu, v, menuInfo) -> c.onCreateContextMenu(menu, v, menuInfo));
    }

    @Override
    public int getItemCount() {
        return internalList.size();
    }

    public Message getMessage(int position) {
        return internalList.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public View container;
        public ImageView contactPic, messageImage;
        public TextView displayname, message;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    public void onDestroy() {
        db.onDestroy();
    }
}
