package com.ccw.picvi.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ccw.picvi.R;
import com.ccw.picvi.interfaces;
import com.ccw.picvi.object.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {

    private final List<Contact> internalList = new ArrayList<>();
    private final LayoutInflater inflater;
    protected interfaces.onItemClickListener l;

    @SuppressWarnings("unused")
    private ContactAdapter(){
        throw new AssertionError();
    }

    public ContactAdapter(Context context, List<Contact> list, interfaces.onItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        internalList.addAll(list);
        l = listener;
    }

    /**
     * Called when this view is created
     * @param parent a reference to the recyclerView parent
     * @param viewType the type of view?
     * @return our view holder
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.recyclerview_row, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.contactPic = itemView.findViewById(R.id.row_picture);
        holder.displayname = itemView.findViewById(R.id.row_name);
        holder.username = itemView.findViewById(R.id.row_snippet);
        holder.container = itemView.findViewById(R.id.row_container);
        return holder;
    }

    /**
     * Called when view is shown on screen
     * @param holder a ViewHolder
     * @param position position
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Contact con = internalList.get(position);
        holder.contactPic.setImageBitmap(BitmapFactory.decodeByteArray(con.picture, 0, con.picture.length));
        holder.displayname.setText(con.displayname);
        holder.username.setText(con.username);
        holder.container.setOnClickListener(v -> l.onItemClick(v, holder.getAdapterPosition()));
    }

    @Override
    public int getItemCount() {
        return internalList.size();
    }

    public Contact getContact(int position) {
        return internalList.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public View container;
        public ImageView contactPic;
        public TextView displayname, username;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
