package com.ccw.picvi.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ccw.picvi.adapter.ContactAdapter;
import com.ccw.picvi.adapter.ConversationAdapter;
import com.ccw.picvi.L;
import com.ccw.picvi.R;
import com.ccw.picvi.intent;
import com.ccw.picvi.interfaces;
import com.ccw.picvi.object.Contact;
import com.ccw.picvi.object.Conversation;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Main extends AppCompatActivity {

    private final List<Conversation> conversationList = new ArrayList<>();

    private BroadcastReceiver receiver;
    private ConversationAdapter adapter;
    private final IntentFilter filter = new IntentFilter(intent.MESSAGE_RECEIVED);
    private RecyclerView rView;
    private FloatingActionButton fab;

    private static final String MIME_TEXT_PLAIN = "text/plain";
    private static final String MIME_IMAGE_ALL = "image/";

    @SuppressLint("WrongThread")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (adapter != null) adapter.notifyDataSetChanged();
            }
        };
        registerReceiver(receiver, filter);

        //TESTCODE
        testSetup();

        Intent i = getIntent();
        if (i != null && i.getAction() != null && i.getData() != null) intentHandler(getIntent());
        else {
            displayContent();
        }

    }

    private void popNewMessage(@Nullable Bundle data) {
        //Open New Message (fragment: compose -> activity: conversation)
        //transition open: slide up | transition close: fade then open conversation
        NewMessageFragment fragment = new NewMessageFragment();
        if (data != null) fragment.setArguments(data);
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left,
                        android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .add(android.R.id.content, fragment, L.TAG)
                .addToBackStack(null)
                .commit();

    }

    public void testSetup() {
        int iteration = -1;
        for (int i=0;i<500;i++) {
            /*
            (3*n)-y=-x
            (3*n)+b=p
            (3*n)=p-b
            (3*n)-p=-b
            x= -x * -1
            n=iteration; p=position=i; b=base;
             */
            if (i % 3 == 0) iteration++;
            int color = ((3 * iteration) - i) * -1;
            Bitmap bm = createBitmap(color);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 20, stream);
            byte[] result = stream.toByteArray();
            bm.recycle();

            String name = color == 0 ? "Red" : color == 1 ? "Green" : "Blue";
            String snip = "n=" +
                    iteration +
                    " p=" +
                    i +
                    " c=" +
                    color;
            Conversation conv = new Conversation(i, name, snip, result);
            conversationList.add(conv);
        }
    }
    public Bitmap createBitmap(int test) {
        Bitmap bitmap = Bitmap.createBitmap(256, 256, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        int color;
        if (test == 0) {
            color = Color.RED;
        }
        else if (test == 1) {
            color = Color.GREEN;
        }
        else {
            color = Color.BLUE;
        }
        canvas.drawColor(color);
        return bitmap;
    }

    private void displayContent() {
        setContentView(R.layout.activity_main);
        fab = findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(v -> popNewMessage(null));

        rView = findViewById(R.id.conversationList);
        rView.setLayoutManager(new LinearLayoutManager(this));
        interfaces.onItemClickListener listener = (v, position) -> {
            Conversation conv = adapter.getConversation(position);
            String toast = "name: " + conv.name + " pos: " + position;
            L.d(toast);
        };
        interfaces.onCreateContextMenuListener cListener = (menu, v, menuInfo) -> {
            //       groupId, itemId, order, title/string res
            menu.add(0, 1, 0, 0);
        };
        adapter = new ConversationAdapter(this, conversationList, listener, cListener);
        rView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 10, 0, "Debug")
                .setIcon(R.mipmap.ic_launcher_round)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 10:
                //TESTCODE
                //open debug screen
                Intent i = new Intent(getApplicationContext(), com.ccw.picvi.activity.Conversation.class);
                i.setData(Uri.parse("picvi://user/admin"));
                startActivity(i);
                return true;
            default: return super.onOptionsItemSelected(item);
        }

    }


    private void openOrCreateConversation(long id) {

    }




    private void intentHandler(Intent i) {
        switch (i.getAction()) {
            case intent.OPEN_CONVERSATION -> openOrCreateConversation(i.getLongExtra(intent.EXTRA_ID, -1));
            case intent.COMPOSE_MESSAGE -> {
                displayContent();
                popNewMessage(i.getExtras());
            }
            case Intent.ACTION_SEND -> {
                if (i.getType() == null) break;
                if (MIME_TEXT_PLAIN.equals(i.getType())) {
                    String text = i.getStringExtra(Intent.EXTRA_TEXT);
                    if (text != null) {

                    }
                } else if (i.getType().startsWith(MIME_IMAGE_ALL)) {
                    Uri imageUri = (Uri) i.getParcelableExtra(Intent.EXTRA_STREAM);
                    if (imageUri != null) {

                    }
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().popBackStackImmediate()) return;
        super.onBackPressed();
    }


    public static class NewMessageFragment extends Fragment {

        private final List<Contact> list = new ArrayList<>();


        @SuppressLint("WrongThread")
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

            //TESTCODE
            Drawable d = ResourcesCompat.getDrawable(getResources(), R.mipmap.ic_launcher, null);
            Bitmap bm = drawableToBitmap(d);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 20, stream);
            byte[] result = stream.toByteArray();
            bm.recycle();

            for (int i=0;i<20;i++) {
                Contact contact = new Contact(i, "Friend" + i, "friend" + i, result);
                list.add(contact);
            }

            return inflater.inflate(R.layout.compose_messsage, parent, false);
        }

        @Override
        public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            RecyclerView conList = view.findViewById(R.id.contact_list);
            conList.setLayoutManager(new LinearLayoutManager(getContext()));
            interfaces.onItemClickListener listener = (v,position) -> {

            };
            ContactAdapter adapter = new ContactAdapter(getContext(), list, listener);
            conList.setAdapter(adapter);
        }

        @Override
        public void onStart() {
            super.onStart();
            Main act = (Main) getActivity();
            assert act != null;
            if (act.rView != null) {
                act.rView.setVisibility(View.GONE);
                act.fab.setVisibility(View.GONE);
            }
        }

        @Override
        public void onStop() {
            super.onStop();
            Main act = (Main) getActivity();
            assert act != null;
            if (act.rView != null && act.rView.getVisibility() == View.GONE) act.rView.setVisibility(View.VISIBLE);
            if (act.fab != null && act.fab.getVisibility() == View.GONE) act.fab.setVisibility(View.VISIBLE);
        }

        public static Bitmap drawableToBitmap (Drawable drawable) {
            Bitmap bitmap;

            if (drawable instanceof BitmapDrawable bitmapDrawable) {
                if(bitmapDrawable.getBitmap() != null) {
                    return bitmapDrawable.getBitmap();
                }
            }

            if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
                bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        }
    }
}