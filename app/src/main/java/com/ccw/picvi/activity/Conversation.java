package com.ccw.picvi.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ccw.picvi.R;
import com.ccw.picvi.adapter.MessageAdapter;
import com.ccw.picvi.intent;

public class Conversation extends AppCompatActivity {

    private BroadcastReceiver receiver;
    private MessageAdapter adapter;
    private final IntentFilter filter = new IntentFilter(intent.MESSAGE_RECEIVED);
    private RecyclerView rView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO use ID over username to prevent any exploits or sanitize username entry
        Uri username = getIntent().getData();
        if (username == null) {
            this.finish();
            return;
        }
        setTitle(username.getLastPathSegment());

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (adapter != null) adapter.notifyDataSetChanged();
            }
        };
        registerReceiver(receiver, filter);
        setContentView(R.layout.activity_conversation);
        rView = findViewById(R.id.conversationList);

        rView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MessageAdapter(this, username, null, null);
        rView.setAdapter(adapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adapter.onDestroy();
    }
}
